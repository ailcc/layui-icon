# 此仓库不在进行更新，可以根据自定义矢量图教程进行自定义操作！

# layui-icon

#### 介绍
在贤心大佬layui的矢量图标基础上，新增了部分矢量图。 <br />
同时，严格按照官方文档添加了以前版本（168个）的原始图标，其中有几个图标实在找不到，就找了相近或类似的。<br />

各位朋友可以下载，然后再layui文件夹中，删除font文件夹下面的文件，然后上传新的。 <br />
再在layui.css中删除矢量图的代码，然后把新下载的iconfont.css中的矢量图代码复制，并粘贴到原位，强制刷新浏览器即可生效<br />

#### 安装教程
1、删除原有layui文件夹下面的font文件夹下面的全部文件，将下载后相同位置的文件上传至font文件夹中。<br /> 
文件列表：<br /> 
iconfont.eot<br />
iconfont.svg<br />
iconfont.ttf<br />
iconfont.woff<br />
iconfont.woff2<br />
2、打开 layui.css 文件，将其中的矢量图css删除（不要删除全部css，仅删除矢量图css） <br />
3、打开下载文件中的iconfont.css 然后将矢量图css复制，并粘贴至layui.css中矢量图css的原位置。 <br />
4、保存layui.css 然后强制刷新浏览器即可。<br />

#### 预览地址

图标预览地址： [https://ailcc.com/static/layui-icon/](https://ailcc.com/static/layui-icon/?_blank)<br />
如需增加其他的矢量图标，请前往上述网站留言，或添加个人主页QQ。<br />

#### DIY图标教程
自定义矢量图标教程
地址：[https://ailcc.com/layui_icon.html](https://ailcc.com/layui_icon.html?_blank)

#### 更新记录
20220912 新增图标34个<br />
20220913 新增图标3个<br />
20220926 新增图标2个 并修复之前版本中 评论图标无法正常显示的问题<br />


#### 免责声明
该项目库仅提供Layui矢量图标修改版下载，不提供其他服务，本人有且只有当前Gitee一个账户。<br />
对Layui 矢量图标库的修改，已经过原作者：贤心 允许及许可，各位可以放心使用。<br />
本人保证之前168个矢量图标严格按照官方文档添加，不影响贵方网站的使用，后续如果官方继续添加、更新图标，本站也会第一时间按照官方文档进行更新。<br />
<br />
但如果贵方在进行图标替换、修改及使用期间出现问题或涉及任何法律纠纷等，一切和本人无关，本站不承担任何法律责任。<br />